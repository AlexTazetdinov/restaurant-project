package com.chdtu.entity;

import javax.persistence.Entity;

@Entity
public class User extends BaseEntity {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
