package com.chdtu.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class TypeOfDish extends BaseEntity{
    private String name;

    @ManyToOne
    Unity unity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Unity getUnity() {
        return unity;
    }

    public void setUnity(Unity unity) {
        this.unity = unity;
    }

    @Override
    public String toString() {
        return getName();
    }
}
