package com.chdtu.entity;

import javax.persistence.Entity;

@Entity
public class Unity extends BaseEntity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getName();
    }
}
