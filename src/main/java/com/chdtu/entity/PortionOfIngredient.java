package com.chdtu.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class PortionOfIngredient extends BaseEntity {

    @ManyToOne
    Ingredient ingredient;

    Long number;

    @ManyToOne
    Recipe recipe;

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
}
