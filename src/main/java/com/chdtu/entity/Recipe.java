package com.chdtu.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class Recipe extends BaseEntity {
    private String fullName;

    private String shortName;

    private Date creationDate;

    @OneToMany(mappedBy = "recipe")
    private Set<PortionOfIngredient> portionOfIngredients;

    @ManyToOne
    private TypeOfDish typeOfDish;


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Set<PortionOfIngredient> getPortionOfIngredients() {
        return portionOfIngredients;
    }

    public void setPortionOfIngredients(Set<PortionOfIngredient> portionOfIngredients) {
        this.portionOfIngredients = portionOfIngredients;
    }

    public TypeOfDish getTypeOfDish() {
        return typeOfDish;
    }

    public void setTypeOfDish(TypeOfDish typeOfDish) {
        this.typeOfDish = typeOfDish;
    }

}
