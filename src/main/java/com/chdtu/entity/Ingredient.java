package com.chdtu.entity;

import javax.persistence.Entity;

@Entity
public class Ingredient extends BaseEntity {

    private String name;

    private long caloricity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCaloricity() {
        return caloricity;
    }

    public void setCaloricity(long caloricity) {
        this.caloricity = caloricity;
    }
}
