package com.chdtu.beans;

import com.chdtu.entity.Recipe;

import java.util.Date;

public class RecipeBean extends Recipe {

    private String typeOfDishName;

    public String getTypeOfDishName() {
        return typeOfDishName;
    }

    public void setTypeOfDishName(String typeOfDishName) {
        this.typeOfDishName = typeOfDishName;
    }
}
