package com.chdtu.vaadin;


import com.chdtu.entity.Recipe;

public class RecipeModifiedEvent {
    private final Recipe recipe;

    public RecipeModifiedEvent(Recipe recipe) {
        this.recipe = recipe;
    }

    public Recipe getRecipe() {
        return recipe;
    }
}

