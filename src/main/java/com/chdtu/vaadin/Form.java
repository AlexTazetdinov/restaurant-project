package com.chdtu.vaadin;

import com.chdtu.repository.Repository;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Window;
import org.vaadin.spring.events.EventBus;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.form.AbstractForm;

import javax.annotation.PostConstruct;
import java.util.List;


public abstract class Form<T> extends AbstractForm<T> {

    private Component[] components;
    private List<FieldConformity> fieldsConformity;
    private EventBus.UIEventBus eventBus;
    private Repository repository;

    public abstract void reload();

    @Override
    public Window openInModalPopup() {
        reload();
        return super.openInModalPopup();
    }

    public Form(Class<T> targetClass, Repository repository, EventBus.UIEventBus b) {
        super(targetClass);
        //this.fieldsConformity = fieldsConformity;
        this.repository = repository;
        this.eventBus = b;
        // generateComponentsFromParameters();
        // On save & cancel, publish events that other parts of the UI can listen
        setSavedHandler(x -> {
            // persist changes
            this.repository.save(x);
            // send the event for other parts of the application
            eventBus.publish(this, new ModifiedEvent(x));
        });
        setResetHandler(x -> eventBus.publish(this, new ModifiedEvent(x)));

        setSizeUndefined();
    }


//    @Override
//    protected Component createContent() {
//        return new MVerticalLayout(
//                new MFormLayout(components).withWidth(""),
//                getToolbar()
//        ).withWidth("");
//    }
//
//    @Override
//    protected void bind() {
//        // DateField in Vaadin 8 uses LocalDate by default, the backend
//        // uses plain old java.util.Date, thus we need a converter, using
//        // built in helper here
//        getBinder()
//                .forMemberField(creationDate)
//                .withConverter(new LocalDateToDateConverter());
//        super.bind();
//    }
//
//    @Override
//    protected Component createContent() {
//        return new MVerticalLayout(
//                new MFormLayout(
//                        fullName,
//                        shortName,
//                        creationDate
//                       // components
//                ).withWidth(""),
//                getToolbar()
//        ).withWidth("");
//    }

    private void generateComponentsFromParameters() {
        components = new Component[fieldsConformity.size()];
        int i = 0;
        for (FieldConformity fieldConformity : fieldsConformity) {
            switch (fieldConformity.getTypeOfField()) {
                case TEXT: {
                    components[i] = new MTextField(fieldConformity.getFieldValue());
                    i++;
                    break;
                }
                case DATE: {
                    components[i] = new DateField(fieldConformity.getFieldValue());
                    i++;
                    break;
                }
            }
        }
    }

    public Component[] getComponents() {
        return components;
    }

    public void setComponents(Component[] components) {
        this.components = components;
    }
}
