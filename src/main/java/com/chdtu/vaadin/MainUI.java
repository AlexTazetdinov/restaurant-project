package com.chdtu.vaadin;

import com.chdtu.vaadin.recipe.RecipeView;
import com.chdtu.vaadin.typeOfDish.TypeOfDishView;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

//@Title("PhoneBook CRUD example")
@Theme("valo")
@SpringUI
public class MainUI<T> extends UI {

    @Autowired
    private SpringViewProvider viewProvider;

    public MainUI() {
    }

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout root = new VerticalLayout();
        HorizontalLayout menuBar = new HorizontalLayout();
        menuBar.addComponent(createNavigationButton("recipe", RecipeView.VIEW_NAME));
        menuBar.addComponent(createNavigationButton("type of dish", TypeOfDishView.VIEW_NAME));

        root.setSizeFull();
        root.setMargin(true);
        root.setSpacing(true);
        root.addComponent(menuBar);
        setContent(root);

        final Panel viewContainer = new Panel();
        viewContainer.setSizeFull();
        root.addComponent(viewContainer);
        root.setExpandRatio(viewContainer, 1.0f);

        Navigator navigator = new Navigator(this, viewContainer);
        navigator.addProvider(viewProvider);
    }

    private Button createNavigationButton(String caption, final String viewName) {
        Button button = new Button(caption);
        button.addStyleName(ValoTheme.BUTTON_SMALL);
        button.addClickListener(event -> getUI().getNavigator().navigateTo(viewName));
        return button;
    }
}
