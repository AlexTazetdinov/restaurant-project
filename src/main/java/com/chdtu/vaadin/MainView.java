package com.chdtu.vaadin;


import com.chdtu.repository.Repository;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Button;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.spring.events.EventBus;
import org.vaadin.spring.events.EventScope;
import org.vaadin.spring.events.annotation.EventBusListenerMethod;
import org.vaadin.viritin.button.ConfirmButton;
import org.vaadin.viritin.button.MButton;
import org.vaadin.viritin.form.AbstractForm;
import org.vaadin.viritin.grid.MGrid;
import org.vaadin.viritin.layouts.MHorizontalLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;
import java.util.List;

public class MainView<T> extends MVerticalLayout implements View {

    Repository repo;
    AbstractForm form;

    @Autowired
    EventBus.UIEventBus eventBus;

    private Class targetClass;
    private MGrid grid;

    //    private MTextField filterByName = new MTextField()
//            .withPlaceholder("Filter by name");
    private Button addNew = new MButton(VaadinIcons.PLUS, this::add);
    private Button edit = new MButton(VaadinIcons.PENCIL, this::edit);
    private Button delete = new ConfirmButton(VaadinIcons.TRASH,
            "Are you sure you want to delete the entry?", this::remove);

    public MainView(Class targetClass, Repository repo, Form form, List<String> propertyNames, List<String> columnNames) {
        this.repo = repo;
        this.form = form;
        this.targetClass = targetClass;
        this.grid = new MGrid<>(targetClass);
        grid.withProperties(propertyNames.toArray(new String[propertyNames.size()]))
                .withColumnHeaders(columnNames.toArray(new String[columnNames.size()]))
                // not yet supported by V8
                //.setSortableProperties("name", "email")
                .withFullWidth();
    }

    @PostConstruct
    protected void init() {
//        MVerticalLayout root = new MVerticalLayout(
//                new MHorizontalLayout(addNew, edit, delete),
//                grid
//        ).expand(grid);
//        setContent(
//                root
//        );
        addComponents(new MVerticalLayout(
                new MHorizontalLayout(addNew, edit, delete),
                grid
        ).expand(grid));

        listEntities();

        grid.asSingleSelect().addValueChangeListener(e -> adjustActionButtonState());
//        filterByName.addValueChangeListener(e -> {
//            listEntities(e.getValue());
//        });

        eventBus.subscribe(this);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }


    protected void adjustActionButtonState() {
        boolean hasSelection = !grid.getSelectedItems().isEmpty();
        edit.setEnabled(hasSelection);
        delete.setEnabled(hasSelection);
    }


    final int PAGESIZE = 45;

    private void listEntities() {
        // A dead simple in memory listing would be:
        // list.setRows(repo.findAll());
        grid.setRows(repo.findAll());
        adjustActionButtonState();

    }

    public void add(Button.ClickEvent clickEvent) {
        try {
            edit((T) targetClass.newInstance());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void edit(Button.ClickEvent e) {
        edit((T) grid.asSingleSelect().getValue());
    }

    public void remove() {
        repo.delete(grid.asSingleSelect().getValue());
        grid.deselectAll();
        listEntities();
    }

    protected void edit(final T entity) {
        form.setEntity(entity);
        form.openInModalPopup();
    }

    @EventBusListenerMethod(scope = EventScope.UI)
    public void onModified(ModifiedEvent event) {
        listEntities();
        form.closePopup();
    }

    private class Container<T> {
        public T createContents(Class<T> clazz) {
            try {
                return clazz.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public Class getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(Class targetClass) {
        this.targetClass = targetClass;
    }

}
