package com.chdtu.vaadin.title;

import com.chdtu.entity.Recipe;
import com.chdtu.repository.RecipeRepository;
import com.chdtu.vaadin.MainView;
import com.chdtu.vaadin.recipe.RecipeForm;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.spring.annotation.SpringView;

import java.util.Arrays;

@Title("Recipes")
@Theme("valo")
@SpringView(name = "")
public class TitleView extends MainView<Recipe>{

    public TitleView(RecipeRepository recipeRepository, RecipeForm form) {
        super(Recipe.class, recipeRepository, form, Arrays.asList("id", "creationDate", "fullName", "shortName", "typeOfDish"),
                Arrays.asList("id", "creation date", "full name", "short name", "type of dish"));
    }
}
