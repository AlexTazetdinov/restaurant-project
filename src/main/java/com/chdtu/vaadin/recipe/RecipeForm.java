package com.chdtu.vaadin.recipe;


import com.chdtu.entity.Recipe;
import com.chdtu.entity.TypeOfDish;
import com.chdtu.repository.RecipeRepository;
import com.chdtu.repository.TypeOfDishRepository;
import com.chdtu.vaadin.Form;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;
import org.vaadin.spring.events.EventBus;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

import javax.annotation.PostConstruct;

@UIScope
@SpringComponent
//@SpringView(name ="lul")
public class RecipeForm extends Form<Recipe> implements View {


    private TextField fullName = new MTextField("Full name");
    private TextField shortName = new MTextField("Short name");
    private DateField creationDate = new DateField("Creation date");
    private ComboBox<TypeOfDish> typeOfDishes;
    private TypeOfDishRepository typeOfDishRepository;

    public RecipeForm(RecipeRepository repository, TypeOfDishRepository typeOfDishRepository, EventBus.UIEventBus b) {
        super(Recipe.class, repository, b);
        this.typeOfDishRepository=typeOfDishRepository;
        typeOfDishes = new ComboBox<TypeOfDish>();
        typeOfDishes.setItems(typeOfDishRepository.findAll());

        getBinder().bind(typeOfDishes, "typeOfDish");
    }

    @Override
    protected Component createContent() {
        return new MVerticalLayout(
                new MFormLayout(
                        fullName,
                        shortName,
                        creationDate,
                        typeOfDishes
                ).withWidth(""),
                getToolbar()
        ).withWidth("");
    }

    @Override
    protected void bind() {
        getBinder()
                .forMemberField(creationDate)
                .withConverter(new LocalDateToDateConverter());
        super.bind();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }



    @PostConstruct
    public void init(){
        typeOfDishes.setItems(typeOfDishRepository.findAll());
    }

    @Override
    public void reload() {
        typeOfDishes.setItems(typeOfDishRepository.findAll());
    }
}
