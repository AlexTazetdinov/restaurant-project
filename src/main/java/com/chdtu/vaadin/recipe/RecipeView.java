package com.chdtu.vaadin.recipe;

import com.chdtu.entity.Recipe;
import com.chdtu.repository.RecipeRepository;
import com.chdtu.vaadin.MainView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.spring.annotation.SpringView;

import java.util.Arrays;

import static com.chdtu.vaadin.recipe.RecipeView.VIEW_NAME;

@Title("Recipes")
@Theme("valo")
@SpringView(name = VIEW_NAME)
public class RecipeView extends MainView<Recipe> {
    public static final String VIEW_NAME = "recipe";

    public RecipeView(RecipeRepository recipeRepository, RecipeForm form) {
        super(Recipe.class, recipeRepository, form, Arrays.asList("id", "creationDate", "fullName", "shortName", "typeOfDish"),
                Arrays.asList("id", "creation date", "full name", "short name", "type of dish"));
    }
}
