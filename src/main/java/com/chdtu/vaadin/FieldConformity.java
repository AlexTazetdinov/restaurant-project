package com.chdtu.vaadin;

public class FieldConformity {

    private String fieldValue;

    private TypeOfField typeOfField;

    public FieldConformity(String fieldValue, TypeOfField typeOfField) {
        this.fieldValue = fieldValue;
        this.typeOfField = typeOfField;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public TypeOfField getTypeOfField() {
        return typeOfField;
    }

    public void setTypeOfField(TypeOfField typeOfField) {
        this.typeOfField = typeOfField;
    }
}
