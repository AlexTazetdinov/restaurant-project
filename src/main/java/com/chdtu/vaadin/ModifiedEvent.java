package com.chdtu.vaadin;

import java.io.Serializable;

public class ModifiedEvent implements Serializable {

    private final Object field;


    public ModifiedEvent(Object field) {
        this.field = field;
    }

    public Object getField() {
        return field;
    }

}
