package com.chdtu.vaadin.typeOfDish;


import com.chdtu.entity.TypeOfDish;
import com.chdtu.entity.Unity;
import com.chdtu.repository.TypeOfDishRepository;
import com.chdtu.repository.UnityRepository;
import com.chdtu.vaadin.Form;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextField;
import org.vaadin.spring.events.EventBus;
import org.vaadin.viritin.fields.MTextField;
import org.vaadin.viritin.layouts.MFormLayout;
import org.vaadin.viritin.layouts.MVerticalLayout;

@UIScope
@SpringComponent
//@SpringView(name ="lul")
public class TypeOfDishForm extends Form<TypeOfDish> implements View {

    private TextField name = new MTextField("Name");
    private ComboBox<Unity> unities;

    public TypeOfDishForm(TypeOfDishRepository typeOfDishRepository, UnityRepository unityRepository, EventBus.UIEventBus b) {
        super(TypeOfDish.class, typeOfDishRepository, b);
        unities = new ComboBox<>();
        unities.setItems(unityRepository.findAll());
        getBinder().bind(unities, "unity");
    }



    @Override
    protected Component createContent() {
        return new MVerticalLayout(
                new MFormLayout(
                        name,
                        unities
                ).withWidth(""),
                getToolbar()
        ).withWidth("");
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }

    @Override
    public void reload() {

    }
}
