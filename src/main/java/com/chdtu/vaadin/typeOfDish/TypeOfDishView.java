package com.chdtu.vaadin.typeOfDish;

import com.chdtu.entity.Recipe;
import com.chdtu.entity.TypeOfDish;
import com.chdtu.repository.TypeOfDishRepository;
import com.chdtu.vaadin.MainView;
import com.chdtu.vaadin.recipe.RecipeForm;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.spring.annotation.SpringView;

import java.util.Arrays;

import static com.chdtu.vaadin.typeOfDish.TypeOfDishView.VIEW_NAME;

@Title("Recipes")
@Theme("valo")
@SpringView(name = VIEW_NAME)
public class TypeOfDishView extends MainView<TypeOfDish> {
    public static final String VIEW_NAME = "typeOfDish";

    public TypeOfDishView(TypeOfDishRepository typeOfDishRepository, TypeOfDishForm form) {
        super(TypeOfDish.class, typeOfDishRepository, form, Arrays.asList("id", "name"),
                Arrays.asList("id", "name"));
    }
}
