package com.chdtu.service;


import java.util.List;

public interface CrudService<T> {
    List<T> findAll();

    T findById(Long id);

    void add(T entity);

    void update(T entity);

    void deleteById(Long id);
}
