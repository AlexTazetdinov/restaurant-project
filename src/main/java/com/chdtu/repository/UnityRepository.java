package com.chdtu.repository;

import com.chdtu.entity.Unity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnityRepository extends Repository<Unity, Long> {
}
