package com.chdtu.repository;


import com.chdtu.entity.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecipeRepository extends Repository<Recipe, Long> {
}
