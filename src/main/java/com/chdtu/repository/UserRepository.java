package com.chdtu.repository;


import com.chdtu.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends Repository<User, Long> {

}
