package com.chdtu.repository;


import com.chdtu.entity.TypeOfDish;
import org.springframework.data.jpa.repository.Query;

public interface TypeOfDishRepository extends Repository<TypeOfDish,Long>{

    @Query("select t FROM TypeOfDish t WHERE t.name = ?1")
    TypeOfDish findByName(String name);
}
