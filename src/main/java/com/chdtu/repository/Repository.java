package com.chdtu.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface Repository<T, ID extends Serializable> extends JpaRepository<T, ID> {

    List<T> findAllBy(Pageable pageable);

   // List<T> findByNameLikeIgnoreCase(String nameFilter);
}
